import React from 'react';
import SInfo from 'react-native-sensitive-info';
import {Text, View, Button} from 'react-native';

import homeStyle from '../styles/homeStyles';
import style from '../styles/main';

import Auth0 from '../utils/auth0';
import {userContainer, calendarContainer} from '../store';

export default ({navigation}) => {
  const {user} = userContainer.useContainer();
  const {dateList} = calendarContainer.useContainer();

  const _logOut = async () => {
    SInfo.deleteItem('accessToken', {});
    SInfo.deleteItem('refreshToken', {});
    SInfo.deleteItem('idToken', {});
    try {
      await Auth0.webAuth.clearSession();
    } catch (error) {
      console.error(error);
    }

    navigation.navigate('Auth');
  };

  return (
    <View style={(style.background, homeStyle.container)}>
      <View style={homeStyle.wrapper}>
        <View>
          <Text style={style.largeHeaderText}>Generelt</Text>
          <Text style={style.smallSubtitleText}>nickname: {user.nickname}</Text>
          <Text style={style.smallSubtitleText}>email: {user.email}</Text>
        </View>

        <View style={style.spacer}>
          <Text style={style.largeHeaderText}>Statistikk</Text>
          <Text style={style.smallSubtitleText}>total timestamps</Text>
          <Text style={style.smallSubtitleText}>{dateList.length}</Text>
        </View>

        <View style={style.spacer} />

        <Button title={'Sign out'} onPress={_logOut} />
      </View>
    </View>
  );
};
