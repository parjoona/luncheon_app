import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0B222E',
  },
  headerTitle: {
    color: '#FFFFFF',
    fontSize: 48,
    fontFamily: 'RobotoSlab-Bold',
  },
  headerSub: {
    color: '#DE9C2B',
    fontSize: 36,
    fontFamily: 'RobotoSlab-Bold',
  },
  loginButton: {
    color: '#000000',
    backgroundColor: '#FFFFFF',
    fontSize: 36,
    padding: 10,
  },
});
