import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  background: {
    backgroundColor: '#0B222E',
    height: '100%',
  },
  largeHeaderText: {
    fontSize: 36,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: 'RobotoSlab-Bold',
  },
  smallSubtitleText: {
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily: 'RobotoSlab-Light',
  },
  redText: {
    color: '#E46053',
  },
  greenText: {
    color: '#5CBDAA',
  },
  spacer: {
    marginTop: 75,
  },
});
